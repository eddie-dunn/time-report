// .prettierrc.js
module.exports = {
  trailingComma: "es5",
  semi: false,
  arrowParens: "always",
}
