function uuidv4() {
  return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
    (
      c ^
      (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))
    ).toString(16)
  );
}

function TimeCard({ value = "", type = "text", label = "foo" }) {
  const id = uuidv4();
  return (
    <>
      {label && <label for={id}>{label}</label>}
      <input id={id} type={type} value={value}></input>
    </>
  );
}

export default TimeCard;
