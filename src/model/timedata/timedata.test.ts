import {
  calcFlex,
  calcTimeWorked,
  calcTotalFlex,
  DayReport,
  getLocalDate,
  getLocalISOTime,
  getLocalTime,
  h2hm,
  h2hmString,
} from "./timedata"
import sampleDataJson from "./timereport.sample.json"
//#region TimeData Flex Calc
// 1. Take timedata object
// 2. calculate flex from object
//#endregion

const report1: DayReport = {
  id: "2020-02-09T19:27:38.624Z", // date of report, UTC
  date: "2020-02-09", // date of report, yyyy-mm-dd
  arrive: "09:00", // arrival time, ISO hh:mm
  leave: "17:00", // departure time, ISO hh:mm
  lunch: 30, // break time in minutes
}

const report2: DayReport = {
  id: "2020-02-09T19:27:38.624Z", // date of report, UTC
  date: "2020-02-09", // date of report, yyyy-mm-dd
  arrive: "09:35", // arrival time, ISO hh:mm
  leave: "19:35", // departure time, ISO hh:mm
  lunch: 55, // break time in minutes
}

test("Calculate worked hours from report 1", () => {
  expect(calcTimeWorked(report1)).toBe(7.5)
})

test("Calculate worked hours from report 2", () => {
  const h = calcTimeWorked(report2)
  const hm = h2hm(h)
  expect(hm).toStrictEqual({ h: 9, m: 5 })
})

test("Calculate flex from report", () => {
  expect(calcFlex(report1)).toBe(-0.5)
})

test("h2hm: positive numbers", () => {
  const hmPos1 = h2hm(0.16666666666666696)
  const hmPos2 = h2hm(2.16666666666666696)
  expect(hmPos1).toStrictEqual({ h: 0, m: 10 })
  expect(hmPos2).toStrictEqual({ h: 2, m: 10 })
})

test("h2hm: negative numbers", () => {
  const hmNeg1 = h2hm(-0.16666666666666696)
  const hmNeg2 = h2hm(-3.16666666666666696)
  expect(hmNeg1).toStrictEqual({ h: 0, m: -10 })
  expect(hmNeg2).toStrictEqual({ h: -3, m: -10 })
})

test("h2hm: correct rounding of numbers", () => {
  const hmNeg1 = h2hm(-1.083333333333333)
  expect(hmNeg1).toStrictEqual({ h: -1, m: -5 })
})

test("Calculate negative flex in report", () => {
  const report: DayReport = {
    id: "id",
    date: "1970-01-22",
    arrive: "08:20",
    leave: "15:00",
    lunch: 60,
  }
  expect(h2hmString(calcFlex(report))).toBe("-2h -20m")
})

test("Calculate flex with no lunch", () => {
  const report: DayReport = {
    id: "id",
    date: "1970-01-22",
    arrive: "08:00",
    leave: "16:00",
    lunch: 0,
  }
  expect(calcFlex(report)).toBe(0)
})

test("Calculate total flex from a list of reports 1", () => {
  expect(calcTotalFlex([report1, report1, report1])).toBe(-1.5)
})

test("Calculate total flex from a list of reports 1", () => {
  expect(h2hm(calcTotalFlex([report1, report2, report1]))).toStrictEqual({
    h: 0,
    m: 5,
  })
})

test("Get local ISO date/time", () => {
  const d = new Date("2020-03-11T11:05:26.510Z")
  expect(getLocalISOTime(d, -60)).toBe("2020-03-11T12:05:26.510")
  expect(getLocalISOTime(d, 0)).toBe("2020-03-11T11:05:26.510")
})

test("Get local date", () => {
  const d = new Date("2020-01-11T11:05:26.510Z")
  expect(getLocalDate(d)).toBe("2020-01-11")
})

test("Get local time", () => {
  const d = new Date("2019-01-11T11:05:26.510Z")
  expect(getLocalTime(d, -60)).toBe("12:05")
  expect(getLocalTime(d, 0)).toBe("11:05")
  expect(getLocalTime(d, 120)).toBe("09:05")
})

test("calcTotalFlex avoids weird rounding errors", () => {
  const sample = Object.values(sampleDataJson.__time_report_data) as DayReport[]
  expect(calcTotalFlex(sample)).toBe(22)
  expect(h2hm(calcTotalFlex(sample))).toStrictEqual({
    h: 22,
    m: 0,
  })
})
