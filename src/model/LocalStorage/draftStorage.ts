import { DayReport } from "model/timedata/timedata";
import { createLocalStorage } from "./localStorage";

export const draftStorage = createLocalStorage<DayReport>("timereport.draft")