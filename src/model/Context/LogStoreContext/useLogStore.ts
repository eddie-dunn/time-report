import { LogStoreContext } from "model/Context/LogStoreContext/LogStore"
import { useContext } from "react"

export function useLogStore() {
  return useContext(LogStoreContext)
}
