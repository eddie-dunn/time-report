export function timeDiff(_start: string, _end: string) {
  const start = _start.split(":")
  const startHour = Number(start[0])
  const startSecond = Number(start[1])

  const end = _end.split(":")
  const endHour = Number(end[0])
  const endSecond = Number(end[1])

  const startDate = new Date(0, 0, 0, startHour, startSecond, 0)
  const endDate = new Date(0, 0, 0, endHour, endSecond, 0)

  let diff = endDate.getTime() - startDate.getTime()
  let hours = Math.floor(diff / 1000 / 60 / 60)
  diff -= hours * 1000 * 60 * 60
  const minutes = Math.floor(diff / 1000 / 60)

  if (hours < 0) {
    hours = 24 + hours
  }

  return (
    (hours <= 9 && hours > 0 ? "0" : "") +
    hours +
    ":" +
    (minutes <= 9 ? "0" : "") +
    minutes
  )
}

export function minutesToTime(minutes: number) {
  let hours = Math.floor(minutes / 60)
  let m = minutes % 60
  return (hours <= 9 ? "0" : "") + hours + ":" + (m <= 9 ? "0" : "") + m
}

export async function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms))
}
