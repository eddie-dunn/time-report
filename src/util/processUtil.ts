export const IS_PRODUCTION = process.env.NODE_ENV === "production"
export const IS_DEVELOP = process.env.NODE_ENV === "development"
