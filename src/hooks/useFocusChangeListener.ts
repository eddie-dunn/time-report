import { useDocumentEventListener } from "hooks/useEventListener"
import { useCallback, useEffect, useState } from "react"

interface IProps {
  onVisible?: (ev?: Event) => void
  onHidden?: (ev?: Event) => void
  onFocusChange?: (ev?: Event) => void
}

function onFocusHandler(
  { onVisible, onHidden, onFocusChange }: IProps = {},
  ev?: Event
) {
  if (document.visibilityState === "visible") {
    console.log("onVisible", new Date().toISOString())
    onVisible?.(ev)
  } else {
    console.warn("onHidden")
    onHidden?.(ev)
  }
  console.log("onFocusChange", new Date().toISOString())
  onFocusChange?.(ev)
}

export function useFocusChangeListener({
  onVisible,
  onHidden,
  onFocusChange,
}: IProps = {}) {
  const name = "visibilitychange"
  const [focused, setFocused] = useState(false)

  const cb = useCallback(
    (ev: Event) =>
      onFocusHandler(
        {
          onVisible: () => {
            setFocused(true)
            onVisible?.()
          },
          onHidden,
          onFocusChange,
        },
        ev
      ),
    [onFocusChange, onHidden, onVisible]
  )

  useEffect(() => {
    document.addEventListener(name, cb)
    return () => document.removeEventListener(name, cb)
  }, [cb])

  return {
    //
    focused,
  }
}

export function useVisibilityChangeListener({
  onVisible,
  onHidden,
  onFocusChange,
}: IProps = {}) {
  const [visible, setVisible] = useState(false)
  useDocumentEventListener("visibilitychange", (ev: Event) =>
    onFocusHandler(
      {
        onVisible: () => {
          setVisible(true)
          onVisible?.(ev)
        },
        onHidden,
        onFocusChange,
      },
      ev
    )
  )

  return {
    //
    visible,
  }
}
