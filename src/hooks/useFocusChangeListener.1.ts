import { useCallback, useEffect, useRef, useState } from "react"

interface IProps {
  onVisible?: (ev?: Event) => void
  onHidden?: (ev?: Event) => void
  onFocusChange?: (ev?: Event) => void
}

function onFocusHandler(
  { onVisible, onHidden, onFocusChange }: IProps = {},
  ev?: Event
) {
  if (document.visibilityState === "visible") {
    console.log("onVisible", new Date().toISOString())
    onVisible?.(ev)
  } else {
    console.warn("onHidden")
    onHidden?.(ev)
  }
  console.log("onFocusChange", new Date().toISOString())
  onFocusChange?.(ev)
}

/*
// Set document.onfocus callback
document.onfocus = (ev) => onFocusHandler()

// Remove document.onfocus callback
document.onfocus = null

// Create 'visibilitychange' callback
const cb = (ev: Event) =>
  onFocusHandler({ onFocusChange: () => console.log("onFocusChange") }, ev)

// Add 'visibilitychange' eventlistener to document
document.addEventListener("visibilitychange", cb)

// Remove 'visibilitychange' eventlistener from document
document.removeEventListener("visibilitychange", cb)
*/

export function useFocusChangeListener({
  onVisible,
  onHidden,
  onFocusChange,
}: IProps = {}) {
  const name = "visibilitychange"
  const [focused, setFocused] = useState(false)

  const cb = useCallback(
    (ev: Event) =>
      onFocusHandler(
        {
          onVisible: () => {
            setFocused(true)
            onVisible?.()
          },
          onHidden,
          onFocusChange,
        },
        ev
      ),
    [onFocusChange, onHidden, onVisible]
  )

  useEffect(() => {
    document.addEventListener(name, cb)
    return () => document.removeEventListener(name, cb)
  }, [cb])

  return {
    //
    focused,
  }
}

export function useFocusChangeListener2(
  { name, onVisible, onHidden, onFocusChange }: IProps & { name: string } = {
    name: "visibilitychange",
  }
) {
  const [focused, setFocused] = useState(false)
  useEventListener(name, (ev) =>
    onFocusHandler(
      {
        onVisible: () => {
          setFocused(true)
          onVisible?.(ev)
        },
        onHidden,
        onFocusChange,
      },
      ev
    )
  )

  return {
    //
    focused,
  }
}

export function useEventListener(
  eventName: string, // event name to listen for
  callback: (e: Event) => void, // callback function to execute when event is triggered
  element = window // element to attach the event listener to
) {
  // create a ref that stores a function to remove the event listener
  const savedCallback = useRef<(e: Event) => void>()

  // update the ref.current value with the callback
  useEffect(() => {
    savedCallback.current = callback
  }, [callback])

  // add event listener
  useEffect(() => {
    const eventListener = (event: Event) => savedCallback.current?.(event)
    element.addEventListener(eventName, eventListener)

    // clean up event listener
    return () => {
      element.removeEventListener(eventName, eventListener)
    }
  }, [eventName, element])
}
