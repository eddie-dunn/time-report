import { useEffect, useRef } from "react"

interface IDocEventMap extends DocumentEventMap {}
export function useDocumentEventListener<T extends keyof IDocEventMap>(
  eventName: T, // event name to listen for
  callback: (e: IDocEventMap[T]) => void // callback function to execute when event is triggered
) {
  // create a ref that stores a function to remove the event listener
  const savedCallback = useRef<(e: IDocEventMap[T]) => void>()

  // update the ref.current value with the callback
  useEffect(() => {
    savedCallback.current = callback
  }, [callback])

  // add event listener
  useEffect(() => {
    const eventListener = (e: IDocEventMap[T]) => savedCallback.current?.(e)
    document.addEventListener(eventName, eventListener)

    // clean up event listener
    return () => {
      document.removeEventListener(eventName, eventListener)
    }
  }, [eventName])
}

export function useElementEventListener<T extends keyof ElementEventMap>(
  eventName: T, // event name to listen for
  callback: (e: ElementEventMap[T]) => void, // callback function to execute when event is triggered
  element: Element // element to attach the event listener to
) {
  // create a ref that stores a function to remove the event listener
  const savedCallback = useRef<(e: ElementEventMap[T]) => void>()

  // update the ref.current value with the callback
  useEffect(() => {
    savedCallback.current = callback
  }, [callback])

  // add event listener
  useEffect(() => {
    const eventListener = (e: ElementEventMap[T]) => savedCallback.current?.(e)
    element.addEventListener(eventName, eventListener)

    // clean up event listener
    return () => {
      element.removeEventListener(eventName, eventListener)
    }
  }, [eventName, element])
}

type TElement = Window | Document | Element
export function useCustomEventListener(
  eventName: string, // event name to listen for
  callback: (e: Event) => void, // callback function to execute when event is triggered
  element: TElement = window // element to attach the event listener to
) {
  // create a ref that stores a function to remove the event listener
  const savedCallback = useRef<(e: Event) => void>()

  // update the ref.current value with the callback
  useEffect(() => {
    savedCallback.current = callback
  }, [callback])

  // add event listener
  useEffect(() => {
    const eventListener = (e: Event) => savedCallback.current?.(e)
    element.addEventListener(eventName, eventListener)

    // clean up event listener
    return () => {
      element.removeEventListener(eventName, eventListener)
    }
  }, [eventName, element])
}
