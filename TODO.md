# TODO

> This file contains planned features, ranked by order of importance. Once a
> feature has been added, its corresponding task should be removed.

## Add expander for setting extra options
There are some options that a user might want to set occasionally.

- Working hours
- Notes

## Add sync with Google Drive
Time report data should be syncable so it can be used on many devices.

## Add pubsub notification system
It should be possible to display simple notifications.

## Add notifications
Some actions warrant notifications:

- When app is cached for offline use
- When a new worklog item is added
- When a worklog item is removed

## Enable arbitrary build environments

Enable arbitrary build environments so that the app can be served from different hosts.

<https://create-react-app.dev/docs/deployment/#customizing-environment-variables-for-arbitrary-build-environments>

- .env.gitlab

  ```
  REACT_API_URL=http://eddie-dunn.gitlab.io/time-report
  ```

- .env.bifrost

  ```
  REACT_API_URL=http://bifrost.mine.bz/time-report
  ```

Other config options can be found here: https://create-react-app.dev/docs/advanced-configuration/

## Cleanup repo
Lots of stuff that came with CRA that should be removed.